package com.unus.service;

import com.unus.model.BaseUser;
import com.unus.model.UserDtoBase;
import com.unus.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional
    @Override
    public BaseUser registerUser(UserDtoBase userDto) {
        BaseUser user = new BaseUser(
                userDto.getUsername(),
                passwordEncoder.encode(userDto.getPassword()),
                "USER"
        );
        return saveUser(user);
    }

    @Override
    public Boolean usernameExists(String username) {
        return this.userRepository.findByUserName(username) != null;
    }

    @Override
    public BaseUser saveUser(BaseUser user) {
        return this.userRepository.save(user);
    }
}
