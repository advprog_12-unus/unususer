package com.unus.service;

import com.unus.model.BaseUser;
import com.unus.model.UserDtoBase;

public interface UserService {
    public BaseUser registerUser(UserDtoBase userDto);

    public Boolean usernameExists(final String email);

    public BaseUser saveUser(BaseUser user);
}
