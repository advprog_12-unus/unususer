package com.unus.controller;

import com.unus.model.UserDtoBase;
import com.unus.service.UserServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/service-user")
public class UserController {
    @Autowired
    private UserServiceImpl userservice;

    @GetMapping("/userDto")
    public UserDtoBase getUserDto(Model model) {
        return new UserDtoBase();
    }

    @PostMapping("/register")
    public void registerUser(@RequestBody UserDtoBase user) {
        userservice.registerUser(user);
    }
}